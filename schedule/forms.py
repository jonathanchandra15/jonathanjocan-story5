from django import forms
from . import models

class DateInput(forms.DateInput):
    input_type = "date"
    # input_formats = ["%d %B %Y"]

class TimeInput(forms.TimeInput):
    input_type = "time"
    # input_formats = ['%H:%M']

class MakeSchedule(forms.ModelForm):
    # tanggal = forms.DateField(widget = forms.DateInput(format = "%d %B %Y"), input_formats = ("%d %B %Y",))
    class Meta:
        model = models.Schedule
        fields = ['nama_kegiatan', 'tempat', 'kategori', 'tanggal', 'jam']

        widgets = {
            'tanggal' : DateInput,
            'jam' : TimeInput
        }

        
   