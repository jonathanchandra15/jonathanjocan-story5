from django.shortcuts import render, redirect
from . import forms
from django.http import HttpResponse
from .models import Schedule
    
def schedule(request):
    models = Schedule.objects.all()
    if request.method == 'POST':
        form = forms.MakeSchedule(request.POST)
        if form.is_valid():
            object_schedule = form.save()
            object_schedule.hari = object_schedule.tanggal.strftime('%A')
            object_schedule.save()
            return redirect('schedule:schedule')
    else:
        form = forms.MakeSchedule()
    return render(request, 'schedule.html', {'form':form, 'models':models})

def delete_schedule(request, id):
    schedule = Schedule.objects.all().get(id = id)
    schedule.delete()
    return redirect('schedule:schedule')