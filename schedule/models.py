from django.db import models

# Create your models here.
class Schedule(models.Model):
    nama_kegiatan = models.CharField(max_length = 100)
    tempat = models.CharField(max_length = 100)
    kategori = models.CharField(max_length = 100)
    tanggal = models.DateField(auto_now=False, auto_now_add=False)
    jam = models.TimeField(auto_now=False, auto_now_add=False)
    hari = models.CharField(max_length = 10)

    def __str__(self):
        return self.nama_kegiatan
 